﻿//1)Абстрактная фабрика
//2)Шаблонный метод
using System;

namespace ConsoleApp34
{
    class Program
    {
        static void Main(string[] args)
        {
            var woodenFactory = new WoodenFactory();
            var metalFactory = new MetalFactory();

            var woodenOrder = new WoodenFurnitureOrder(woodenFactory);
            var metalOrder = new MetalFurnitureOrder(metalFactory);

            Console.WriteLine("Заказываем деревянную мебель:");
            woodenOrder.OrderFurniture();

            Console.WriteLine("Заказываем металлическую мебель:");
            metalOrder.OrderFurniture();
        }
    }
    //1)Абстрактная фабрика
    public interface IAbstractFactory
    {
        IChair CreateChair();
        ITable CreateTable();
    }

    public interface IChair
    {
        string GetName();
    }

    public interface ITable
    {
        string GetName();
    }
    public class WoodenFactory : IAbstractFactory
    {
        public IChair CreateChair()
        {
            return new WoodenChair();
        }

        public ITable CreateTable()
        {
            return new WoodenTable();
        }
    }

    public class MetalFactory : IAbstractFactory
    {
        public IChair CreateChair()
        {
            return new MetalChair();
        }

        public ITable CreateTable()
        {
            return new MetalTable();
        }
    }
    public class WoodenChair : IChair
    {
        public string GetName()
        {
            return "Деревянный стул";
        }
    }

    public class MetalChair : IChair
    {
        public string GetName()
        {
            return "Металлический стул";
        }
    }

    public class WoodenTable : ITable
    {
        public string GetName()
        {
            return "Деревянный стол";
        }
    }

    public class MetalTable : ITable
    {
        public string GetName()
        {
            return "Металлический стол";
        }
    }
    //2)Шаблонный метод
    public abstract class FurnitureOrderTemplate
    {
        protected IAbstractFactory _factory;

        public FurnitureOrderTemplate(IAbstractFactory factory)
        {
            _factory = factory;
        }

        public void OrderFurniture()
        {
            // Создаем предметы мебели с помощью фабрики
            var chair = _factory.CreateChair();
            var table = _factory.CreateTable();

            // Вызываем методы, описывающие процесс изготовления
            PrepareMaterials();
            AssembleParts(chair, table);
            ApplyFinish();
        }

        protected abstract void PrepareMaterials();

        protected virtual void AssembleParts(IChair chair, ITable table)
        {
            Console.WriteLine($"Собираем {chair.GetName()} и {table.GetName()}");
        }

        protected abstract void ApplyFinish();
    }
    public class WoodenFurnitureOrder : FurnitureOrderTemplate
    {
        public WoodenFurnitureOrder(IAbstractFactory factory) : base(factory)
        {
        }

        protected override void PrepareMaterials()
        {
            Console.WriteLine("Подготавливаем дерево для изготовления мебели");
        }

        protected override void ApplyFinish()
        {
            Console.WriteLine("Наносим лак на деревянную мебель");
        }
    }

    public class MetalFurnitureOrder : FurnitureOrderTemplate
    {
        public MetalFurnitureOrder(IAbstractFactory factory) : base(factory)
        {
        }

        protected override void PrepareMaterials()
        {
            Console.WriteLine("Подготавливаем металл для изготовления мебели");
        }

        protected override void ApplyFinish()
        {
            Console.WriteLine("Покрываем металлическую мебель краской");
        }
    }

}
